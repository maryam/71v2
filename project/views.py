# Views

from flask import Blueprint, render_template, redirect, url_for
from flask_security import current_user, auth_required

from .models import User, Role

views_bp = Blueprint('views', __name__)

@views_bp.route('/')
def home():
    return render_template('index.html')


@views_bp.route('/profile')
@auth_required()
def profile():
    if not current_user.is_authenticated:
        return redirect(url_for('security.login'))
    else:
        return render_template('profile.html', name=current_user.email)

''' @app.route('/users', endpoint='users', methods=['GET', 'POST'])
@auth_required
def users():
    form = CreateUserForm()
    if form.validate_on_submit():
        user = User(email=form.email.data, password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('User created successfully!', 'success')
        return redirect(url_for('users'))

    users = User.query.all()

    return render_template('users.html', users=users, form=form) '''