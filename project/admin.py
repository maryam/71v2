from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_security import Security, current_user
from wtforms.fields import PasswordField

from flask import current_app
from database import db_session
from models import User, Role

#<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.4/css/bulma.css" integrity="sha512-SI0aF82pT58nyOjCNfyeE2Y5/KHId8cLIX/1VYzdjTRs0HPNswsJR+aLQYSWpb88GDJieAgR4g1XWZvUROQv1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />


bulma = ["https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.4/css/bulma.css"]
current_app.config['FLASK_ADMIN_EXTRA_CSS'] = bulma
admin = Admin(current_app, name='Admin', template_mode='bootstrap4')

class UserModelView(ModelView):
    column_list = ['email','role']
    create_modal = False
    can_view_details = True
    extra_css = ["https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.4/css/bulma.css"]

    def is_accessible(self):
        return current_user.is_authenticated
    
    def inaccessible_callback(self, name, **kwargs): 
        return redirect(url_for('security.login', next=request.url))

    def scaffold_form(self):
        form_class = super(UserModelView, self).scaffold_form()
        form_class.password2 = PasswordField('New Password')
        return form_class
    def on_model_change(self, form, model, is_created):
        if len(model.password2):
            model.password = utils.encrypt_password(model.password2)

    def get_save_return_url(self, model, is_created):
        return self.get_url('.details_view', id=model.id)

    # define a context processor for merging flask-admin's template context into the
    # flask-security views.
    security = current_app.security
    @security.context_processor
    def security_context_processor():
        def get_url(admin, **kwargs):
            return url_for(admin, **kwargs)
        return dict(
            admin_base_template=admin.base_template,
            admin_view=admin.index_view,
            h=admin_helpers,
            get_url=url_for
        )


admin.add_view(UserModelView(User, db_session))          
