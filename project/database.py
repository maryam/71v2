from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from flask import current_app

Base = declarative_base()

def init_db():
    global db_session

    DATABASE_URL = current_app.config['SQLALCHEMY_DATABASE_URI']
    print("inside init_db, database_url : " + str(DATABASE_URL))
    
    engine = create_engine(DATABASE_URL, pool_size=10, max_overflow=20)
    db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

    Base.query = db_session.query_property()
    
    from . import models
    Base.metadata.create_all(bind=engine)

    return db_session
