document.querySelectorAll('[id^="editButton"]').forEach(function(editButton) {
    editButton.addEventListener('click', function() {
        // Extract workshop id from the button id
        var recordId = this.id.replace('editButton', '');

        // Toggle form fields between read-only and editable
        var form = document.getElementById('detailsForm' + recordId);
        if (!form) {
            form = document.querySelector('[id^="evalForm"]');
        }
        for (var i = 0; i < form.elements.length; i++) {
            var element = form.elements[i];
            if (element.hasAttribute('readOnly')) {
                element.readOnly = !element.readOnly
            }
        }

        

        // Toggle visibility of edit and save buttons
        document.getElementById('editButton' + recordId).style.display = form.elements[0].readOnly ? 'block' : 'none';
        document.getElementById('saveButton' + recordId).style.display = form.elements[0].readOnly ? 'none' : 'block';
    });
});
