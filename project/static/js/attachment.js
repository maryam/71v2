let oldImages = {};
let newImages = {};

function toggleThumbnail(attachmentId, fieldName) {
  const redCross = document.getElementById(`red_cross_${fieldName}_${attachmentId}`);
  const removeLabel = document.getElementById(`remove_label_${fieldName}_${attachmentId}`);
  const inputElement = document.getElementById(`${fieldName}_${attachmentId}`);
  if (redCross.style.display === "none") {
    redCross.style.display = "block";
    removeLabel.innerText = "Keep";
    inputElement.disabled = true;
  } else {
    redCross.style.display = "none";
    removeLabel.innerText = "Remove";
    inputElement.disabled = false;
  };
};


function removeNewThumbnail(inputId, fieldName) {
  const columnElement = document.getElementById(`column_${inputId}`);
  columnElement.remove();
};

const imagesCounters = {};
function addImage(fieldName, columnsId) {
  if (!imagesCounters.hasOwnProperty(fieldName)) {
    imagesCounters[fieldName] = 0;
  }
  imagesCounters[fieldName]++;

  const columns = document.getElementById(columnsId);
  const inputId = `${fieldName}_${imagesCounters[fieldName]}`;
  const thumbnailId = `new_${fieldName}_thumbnail_${imagesCounters[fieldName]}`;
  const removeLabelId = `remove_label_${fieldName}_${imagesCounters[fieldName]}`;

  // Create a new column element
  const newColumn = document.createElement('div');
  newColumn.setAttribute('class', 'column is-one-quarter');
  newColumn.setAttribute('id', `column_${inputId}`);

  // Create a new card element
  const newCard = document.createElement('div');
  newCard.setAttribute('class', 'card');

  // Create a new card image element
  const newCardImage = document.createElement('div');
  newCardImage.setAttribute('class', 'card-image');

  // Create a new figure element
  const newFigure = document.createElement('figure');
  newFigure.setAttribute('class', 'image is-4by3');

  // Create a new thumbnail element
  const newThumbnail = document.createElement('img');
  newThumbnail.setAttribute('class', 'thumbnail');
  newThumbnail.setAttribute('id', thumbnailId);

  // Create new thumbnail link
  const newAThumbnail = document.createElement('a');
  newAThumbnail.setAttribute('href', '#');
  newAThumbnail.setAttribute('target', '_blank');
  newAThumbnail.appendChild(newThumbnail);

  // Add elements to the figure
  newFigure.appendChild(newAThumbnail);

  // Add elements to the card image
  newCardImage.appendChild(newFigure);

  // Create a new card footer element
  const newCardFooter = document.createElement('div');
  newCardFooter.setAttribute('class', 'card-footer');

  const newRemoveLabelDiv = document.createElement('div');
  newRemoveLabelDiv.setAttribute('class', 'button card-footer-item');
  newRemoveLabelDiv.setAttribute('id', removeLabelId);
  newRemoveLabelDiv.setAttribute('data-attachment-id', inputId);
  newRemoveLabelDiv.innerText = 'Remove';

  // Add elements to the card footer
  newCardFooter.appendChild(newRemoveLabelDiv);

  // Add elements to the card
  newCard.appendChild(newCardImage);
  newCard.appendChild(newCardFooter);

  // Add elements to the new column
  newColumn.appendChild(newCard);

  // Append the new column element to the columns container
  columns.appendChild(newColumn);

  // Trigger the "click" event on the file input
  const newInput = document.createElement('input');
  newInput.setAttribute('id', `${inputId}`);
  newInput.setAttribute('type', 'file');
  newInput.setAttribute('data-field', `${fieldName}`)
  newInput.style.display = 'none';
  newInput.setAttribute('name', `${inputId}`);
  newCard.appendChild(newInput);
  newInput.click();

  window.addEventListener('focus', function handleFocus() {
    window.removeEventListener('focus', handleFocus); // Remove the event listener to prevent it from triggering multiple times
  
    setTimeout(() => {
      const file = newInput.files[0];
      if (file) {
        const reader = new FileReader();
        reader.onload = function (e) {
          // Set the thumbnail source
          newThumbnail.src = e.target.result;
          newAThumbnail.href = e.target.result;
  
          // Add a click event listener to the remove label
          newRemoveLabelDiv.addEventListener('click', function (event) {
            event.preventDefault();
            removeNewThumbnail(inputId, fieldName);
          });
        };
        reader.readAsDataURL(file);
      } else {
        // Remove the div and input if the file dialog is canceled
        removeNewThumbnail(inputId);
      }
  
      // Remove the input element after processing the file
      //newInput.remove();
    }, 100); // Add a 100ms delay
  });  
};

// Add a click event listener for each add-image-label element
document.querySelectorAll('[id^="add-image-label"]').forEach(function (element) {
  element.addEventListener('click', function (event) {
    event.preventDefault();
    const fieldName = element.getAttribute('data-field');
    const columnsContainer = element.getAttribute('data-columns-container');
    addImage(fieldName, columnsContainer);
  });
});

// Add event listeners for existing images fetched from Airtable
document.querySelectorAll('.existing-remove-label').forEach(function (removeLabel) {
  const fieldName = removeLabel.getAttribute('data-field');
  const attachmentId = removeLabel.getAttribute('data-attachment-id');
  
  removeLabel.addEventListener('click', function (event) {
    event.preventDefault();
    toggleThumbnail(attachmentId, fieldName);
  });
});