function toggleThumbnail(participantId) {
  const redCross = document.getElementById(`red_cross_Participant_${participantId}`);
  const removeLabel = document.getElementById(`remove_label_Participant_${participantId}`);
  const presentParticipantsInput = document.getElementById("presentParticipantsInput");

  if (redCross.style.display === "none") {
    redCross.style.display = "block";
    removeLabel.innerText = "Est présent";
    participants_presents = participants_presents.filter(id => id !== participantId);
  } else {
    redCross.style.display = "none";
    removeLabel.innerText = "Est absent";
    participants_presents.push(participantId)
  };
  presentParticipantsInput.value = Object.keys(participants_presents).join(",");
};

document.getElementById(`presencesForm${ workshopId }`).addEventListener('submit', function() {

    document.getElementById('presentParticipantsInput').value = participants_presents.join(',');
});


